from django.test import TestCase, Client
from django.urls import resolve
from .views import hello_world

# Create your tests here.
class HelloWorldUnitTest(TestCase):
    def test_landing_page_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_landing_views(self):
        found = resolve('/')
        self.assertEqual(found.func, hello_world)

    def test_landing_page_return_json_response(self):
        response = Client().get('')
        self.assertJSONEqual(response.content, {'string':'Hello World!'})