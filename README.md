# Walkiddie Toys -BackEnd

[![pipeline status](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/walkiddie.toys/walkiddie-toys-backend/badges/staging/pipeline.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/walkiddie.toys/walkiddie-toys-backend/-/commits/staging)
[![coverage report](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/walkiddie.toys/walkiddie-toys-backend/badges/staging/coverage.svg)](https://gitlab.cs.ui.ac.id/ppl-fasilkom-ui/2021/CC/walkiddie.toys/walkiddie-toys-backend/-/commits/staging)

## Kontributor

Nama kelompok : Narai
Kelas : C
Anggota :
- Aji Inisti Udma Wijaya - 1806141126
- Muhamad Adhytia Wana Putra Rahmadhan - 1806141321
- Muhammad Ihsan Azizi - 1806186774
- Naufal Alauddin Hilmi - 1806205754 
- Ronaldi Tjaidianto - 1806141441